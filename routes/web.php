<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\IpUtils;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('admin')->middleware(['auth', 'user.route.access', 'http.secure', 'verify.csrf.token', 'start.session', 'share.errors.from.session'])->group(
    function () {

        //тарифы
        Route::get('tariffs', 'admin\TariffController@index')
            ->name('admin.tariffs.index');
        Route::get('tariffs/edit/{id}', 'admin\TariffController@edit')
            ->name('admin.tariffs.edit');
        Route::post('tariffs/delete', 'admin\TariffController@delete')
            ->name('admin.tariffs.delete');
        Route::post('tariffs/save', 'admin\TariffController@save')
            ->name('admin.tariffs.save');
        Route::get('tariffs/add', 'admin\TariffController@edit')
            ->name('admin.tariffs.add');

        //тарифы переводы
        Route::get('tariff-translation', 'admin\TariffTranslationController@index')
            ->name('admin.tariff_translation.index');
        Route::get('tariff-translation/{language}', 'admin\TariffTranslationController@translationList')
            ->name('admin.tariff_translation.translation_list');
        Route::get('tariff-translation/{language}/edit', 'admin\TariffTranslationController@edit')
            ->name('admin.tariff_translation.edit');
        Route::post('tariff-translation/save', 'admin\TariffTranslationController@save')
            ->name('admin.tariff_translation.save');
        Route::get('site-tariffs', 'admin\TariffTranslationController@siteTariffs')
            ->name('admin.tariffs.site_tariffs');

        //типы тарифов
        Route::get('tariff-types', 'admin\TariffTypeController@index')
            ->name('admin.tariff_types.index');
        Route::get('tariff-types/edit/{id}', 'admin\TariffTypeController@edit')
            ->name('admin.tariff_types.edit');
        Route::post('tariff-types/delete', 'admin\TariffTypeController@delete')
            ->name('admin.tariff_types.delete');
        Route::post('tariff-types/save', 'admin\TariffTypeController@save')
            ->name('admin.tariff_types.save');
        Route::get('tariff-types/add', 'admin\TariffTypeController@edit')
            ->name('admin.tariff_types.add');

        //переводы типов тарифов
        Route::get('tariff-types-translation', 'admin\TariffTypeTranslationController@index')
            ->name('admin.tariff_types_translation.index');
        Route::get('tariff-types-translation/{language}', 'admin\TariffTypeTranslationController@translationList')
            ->name('admin.tariff_types.translation_list');
        Route::get('tariff-types-translation/{language}/edit', 'admin\TariffTypeTranslationController@edit')
            ->name('admin.tariff_types_translation.edit');
        Route::post('tariff-types-translation/save', 'admin\TariffTypeTranslationController@save')
            ->name('admin.tariff_types_translation.save');

    }

);

